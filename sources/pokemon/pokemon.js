"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
let offset = 0;
const pokeUrl = "https://pokeapi.co/api/v2/pokemon/";
const appendUrl = "?limit=20&offset=";
(function initialize() {
    return __awaiter(this, void 0, void 0, function* () {
        const pokelist = yield $.get(pokeUrl);
        let html = getPokeApi(pokelist);
        offset = 20;
        $('#pokemons')[0].innerHTML = html;
    });
})();
function getPokeApi(pokelist) {
    let html = '';
    for (const pokemon of pokelist.results) {
        html += `<h4 class="has-text-black">${pokemon.name}</h4><button class="button is-danger" onclick=listDetails('${pokemon.url}')>Details</button>`;
    }
    return html;
}
function navigate(navigation) {
    return __awaiter(this, void 0, void 0, function* () {
        if (navigation == 1) {
            const pokelist = yield $.get(pokeUrl + appendUrl + offset);
            const html = getPokeApi(pokelist);
            $('#pokemons')[0].innerHTML = html;
            offset += 20;
        }
        else if (navigation == 2) {
            if (offset > 0) {
                offset -= 20;
            }
            const pokelist = yield $.get(pokeUrl + appendUrl + offset);
            const html = getPokeApi(pokelist);
            $('#pokemons')[0].innerHTML = html;
        }
    });
}
function listDetails(url) {
    return __awaiter(this, void 0, void 0, function* () {
        let modal = document.getElementById("myModal");
        if (modal != null) {
            modal.style.display = "block";
            window.onclick = function (event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            };
        }
        let pokeName = document.getElementById("pokeName");
        let pokeWeight = document.getElementById("pokeWeight");
        let pokeImage = document.getElementById("pokeImage");
        let pokeAbilities = document.getElementById("pokeAbilities");
        const response = yield fetch(url);
        const pokemon = yield response.json();
        if (pokeName != null) {
            pokeName.innerHTML = pokemon.name;
        }
        if (pokeWeight != null) {
            pokeWeight.innerHTML = "Weight: " + pokemon.weight;
        }
        if (pokeImage != null) {
            pokeImage.innerHTML = "";
            let image = document.createElement("img");
            image.setAttribute("id", "image");
            image.setAttribute("src", pokemon.sprites.front_default);
            pokeImage.appendChild(image);
        }
        if (pokeAbilities != null) {
            pokeAbilities.innerHTML = "";
            let abilities = "<h2> Abilities </h2><br/>";
            for (const ability of pokemon.abilities) {
                abilities += ability.ability.name + "<br/>";
            }
            pokeAbilities.innerHTML = abilities;
        }
    });
}
