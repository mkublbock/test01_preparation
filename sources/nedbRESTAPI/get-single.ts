import {Next, Request, Response} from 'restify';
import {BadRequestError, NotFoundError} from 'restify-errors';
//import {customers} from './data';
import * as Datastore from 'nedb';
import {db} from './server';

export function getSingle(req: Request, res: Response, next: Next):void{
    const searchedId = parseInt(req.params.id);
    if(searchedId)
    {
        db.find({id: searchedId}, (err, customers) => {
            if(customers)
            {
                res.send(customers);
            }else
            {
                next(new NotFoundError());
            }     
        });
        /* ohne nedb:
        const customer = db.find(c => c.id === id);
        if(customer){
            res.send(customer);
            next();
        }else{
            next(new NotFoundError());
        }*/
    }else
    {
        next(new BadRequestError('Parameter id must be a number'));
    }
}


