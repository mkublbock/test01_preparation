async function getCustomerList() {
    const fetchResponse = await fetch("http://localhost:8080/api/customers");
    const fetchResult = await fetchResponse.json();

    console.log(fetchResult);
    let customerTable: string = "<table class='table'>";
    for(let i =0;i<fetchResult.length;i++)
    {
        console.log(fetchResult[i].id);
        console.log(fetchResult[i].firstName);
        customerTable = customerTable + `<tr><td>${fetchResult[i].id}</td><td>${fetchResult[i].firstName}</td><td>${fetchResult[i].lastName}</td></tr>`;

    }
    customerTable = customerTable + "</table>";
    $("#customerListContainer").html(customerTable);

}

