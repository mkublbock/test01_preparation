"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const restify_errors_1 = require("restify-errors");
const server_1 = require("./server");
function deleteSingle(req, res, next) {
    const searchedId = parseInt(req.params.id);
    if (searchedId) {
        server_1.db.remove({ id: searchedId }, { multi: true }, (err, numRemoved) => {
            if (numRemoved > 0) {
                res.send(204);
            }
            else {
                next(new restify_errors_1.NotFoundError());
            }
        });
    }
    else {
        next(new restify_errors_1.BadRequestError('Parameter id must be a number'));
    }
}
exports.deleteSingle = deleteSingle;
