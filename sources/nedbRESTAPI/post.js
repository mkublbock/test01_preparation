"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_codes_1 = require("http-status-codes");
const restify_errors_1 = require("restify-errors");
const server_1 = require("./server");
function post(req, res, next) {
    if (!req.body.id || !req.body.firstName || !req.body.lastName) {
        next(new restify_errors_1.BadRequestError('Missing mandatory member(s)'));
    }
    else {
        const newCustomerId = parseInt(req.body.id);
        if (!newCustomerId) {
            next(new restify_errors_1.BadRequestError('ID has to be a numeric value'));
        }
        else {
            server_1.db.insert({ id: newCustomerId, firstName: req.body.firstName, lastName: req.body.lastName }, (err, newDoc) => {
                res.send(http_status_codes_1.CREATED, newDoc, { Location: `${req.path()}/${req.body.id}` });
            });
            next();
            /* ohne nedb:
            const newCustomer: ICustomer = {id: newCustomerId,
            firstName: req.body.firstName,
            lastName: req.body.lastName};
            customers.push(newCustomer);
            res.send(CREATED, newCustomer, {Location: `${req.path()}/${req.body.id}`})*/
        }
    }
}
exports.post = post;
