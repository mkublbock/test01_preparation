import {createServer, plugins} from 'restify';
import * as Datastore from 'nedb';

import {deleteSingle} from './delete-single';
import {getAll} from './get-all';
import {getSingle} from './get-single';
import {post} from './post';

var server = createServer();

// Add bodyParser plugin for parsing JSON in request body
server.use(plugins.bodyParser());
server.use(
    function crossOrigin(req,res,next){
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      return next();
    }
  );

export const db = new Datastore({filename: __dirname + '/db.dat', autoload:true});

// Add routes
server.get('/api/customers', getAll);
server.post('/api/customers', post);
server.get('/api/customers/:id', getSingle);
server.del('/api/customers/:id', deleteSingle);


server.listen(8080, () => console.log('API is listening'));
