import {Next, Request, Response} from 'restify';
import {BadRequestError, NotFoundError} from 'restify-errors';
import * as Datastore from 'nedb';
import {db} from './server';

export function deleteSingle(req:Request, res:Response, next:Next):void{
    const searchedId = parseInt(req.params.id);
    if(searchedId)
    {
        db.remove({id: searchedId}, {multi: true}, (err, numRemoved) => {
            if(numRemoved>0)
            {
                res.send(204);
            }else
            {
                next(new NotFoundError());
            }     
        });
    }else
    {
        next(new BadRequestError('Parameter id must be a number'));
    }
}

