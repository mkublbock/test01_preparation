"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = require("./server");
function getAll(req, res, next) {
    server_1.db.find({}, (err, customers) => {
        res.send(customers);
    });
    next();
}
exports.getAll = getAll;
