var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
function getCustomerList() {
    return __awaiter(this, void 0, void 0, function* () {
        const fetchResponse = yield fetch("http://localhost:8080/api/customers");
        const fetchResult = yield fetchResponse.json();
        console.log(fetchResult);
        let customerTable = "<table class='table'>";
        for (let i = 0; i < fetchResult.length; i++) {
            console.log(fetchResult[i].id);
            console.log(fetchResult[i].firstName);
            customerTable = customerTable + `<tr><td>${fetchResult[i].id}</td><td>${fetchResult[i].firstName}</td><td>${fetchResult[i].lastName}</td></tr>`;
        }
        customerTable = customerTable + "</table>";
        $("#customerListContainer").html(customerTable);
    });
}
