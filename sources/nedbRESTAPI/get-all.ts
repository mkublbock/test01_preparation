import {Request, Response, Next} from 'restify';
//import {customers} from './data';
import * as Datastore from 'nedb';
import {BadRequestError, UnauthorizedError} from 'restify-errors';
import {db} from './server';

export function getAll(req: Request, res:Response, next: Next):void{
    db.find({}, (err, customers) => {
        res.send(customers);
    });
    next();
}
