"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const restify_errors_1 = require("restify-errors");
const server_1 = require("./server");
function getSingle(req, res, next) {
    const searchedId = parseInt(req.params.id);
    if (searchedId) {
        server_1.db.find({ id: searchedId }, (err, customers) => {
            if (customers) {
                res.send(customers);
            }
            else {
                next(new restify_errors_1.NotFoundError());
            }
        });
        /* ohne nedb:
        const customer = db.find(c => c.id === id);
        if(customer){
            res.send(customer);
            next();
        }else{
            next(new NotFoundError());
        }*/
    }
    else {
        next(new restify_errors_1.BadRequestError('Parameter id must be a number'));
    }
}
exports.getSingle = getSingle;
