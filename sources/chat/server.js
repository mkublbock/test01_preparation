"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const http = require("http");
const sio = require("socket.io");
const app = express();
const server = http.createServer(app);
const io = sio(server);
const port = process.env.PORT || 3000;
server.listen(port, function () {
    console.log('Webserver listens on Port %d', port);
});
app.use(express.static(__dirname));
io.on('connection', function (socket) {
    let addedUser = false;
    let username = '';
    socket.on('add user', function (newUserName) {
        username = newUserName;
        addedUser = true;
        socket.emit('login');
        socket.broadcast.emit('user joined', username);
    });
    socket.on('new message', function (data) {
        socket.broadcast.emit('new message', { username: username, message: data });
    });
    socket.on('disconnect', function () {
        if (addedUser) {
            socket.broadcast.emit('user left', username);
        }
    });
});
