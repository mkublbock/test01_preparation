# Vorbereitung für den 1.Test in Mobile-Computing

## How to start

* clone this repo
* type "npm install"
* type "npm run-script build"
* type "npm run-script start"
* Have Fun!

# Initialisieren des Projekts

 * 'npm init' --> 'package.json'
 * 'tsc --init' --> 'tsconfig.json'
 * browser-sync: "start": "browser-sync start --server --files '*.html' '*.js' "
 * browser-sync und watch: "start": "concurrently \"tsc --watch app.ts\" \"browser-sync start --server --files 'index.html' 'app.js'\""
 * build: tsc 
 

# devDependencies vs Dependencies

* dependencies are required to run, devDependencies only to develop
* --save-dev
* npm install typescript --save-dev
* npm install browser-sync --save-dev
* npm install concurrently --save-dev

# Links

* Slides: https://rstropek.github.io/htl-mobile-computing/#/3/4
* Typescript-types: http://www.typescriptlang.org/docs/handbook/basic-types.html
* Typescript-Functions: http://www.typescriptlang.org/docs/handbook/functions.html
* Typescript-Interfaces: http://www.typescriptlang.org/docs/handbook/interfaces.html
* Typescript-Classes: http://www.typescriptlang.org/docs/handbook/classes.html
* Typescript-Iterators and Generators: http://www.typescriptlang.org/docs/handbook/iterators-and-generators.html
* Typescript-Modules: http://www.typescriptlang.org/docs/handbook/modules.html


# Fehler und Lösungen

*  (ReferenceError): $ is not defined = jquery script in html einbinden --> <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
* exports not defined: Laut Stropek kommt das beim Test nicht
* kein Modul gefunden für xxx: npm install @types/xxx --save-dev

